import { setClientError, setServerError } from './errorReducer';

export const setErrorMessage = (errorMessage, errorStatus) => {
  return (dispatch) => {
    if (errorStatus >= 500) {
      return dispatch(setServerError());
    }

    if (errorStatus >= 400) {
      return dispatch(setClientError(errorMessage));
    }

    if (errorMessage) {
      return dispatch(setClientError(errorMessage));
    }
  };
};
