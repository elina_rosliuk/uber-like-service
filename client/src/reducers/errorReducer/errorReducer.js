const SERVER_ERROR = 'SERVER_ERROR';
const CLIENT_ERROR = 'CLIENT_ERRORG';
const REMOVE_ERROR = 'REMOVE_ERROR';

const defaultState = {
  error: null,
};

export default function loadingReducer(state = defaultState, action) {
  switch (action.type) {
    case SERVER_ERROR:
      return {
        ...state,
        error: 'Server error',
      };
    case CLIENT_ERROR:
      return {
        ...state,
        error: action.payload || 'Client error',
      };
    case REMOVE_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
}

export const setServerError = () => ({
  type: SERVER_ERROR,
});
export const setClientError = (message) => ({
  type: CLIENT_ERROR,
  payload: message,
});
export const removeError = () => ({
  type: REMOVE_ERROR,
});
