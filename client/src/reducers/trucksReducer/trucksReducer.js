const SET_TRUCKS = 'SET_TRUCKS';

const defaultState = {
  trucks: [],
};

export default function trucksReducer(state = defaultState, action) {
  switch (action.type) {
    case SET_TRUCKS:
      return {
        ...state,
        trucks: action.payload,
      };
    default:
      return state;
  }
}

export const setTrucks = (trucks) => ({ type: SET_TRUCKS, payload: trucks });
