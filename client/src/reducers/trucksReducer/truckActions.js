import axios from 'axios';
import { setErrorMessage } from '../errorReducer/errorActions';
import { setIsLoading } from '../loadingReducer/loadingReducer';
import { setTrucks } from './trucksReducer';

export const getTrucks = () => {
  return async (dispatch) => {
    dispatch(setIsLoading(true));
    try {
      const response = await axios.get(`/api/trucks`);
      dispatch(setTrucks(response.data.trucks));
    } catch (err) {
      dispatch(setIsLoading(false));
      console.log(err);
    }
    dispatch(setIsLoading(false));
  };
};

export const deleteTruck = (id) => {
  return async (dispatch) => {
    try {
      await axios.delete(`/api/trucks/${id}`);
      await dispatch(getTrucks());
    } catch (err) {
      console.log(err);

      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const assignTruck = (id) => {
  return async (dispatch) => {
    try {
      await axios.post(`/api/trucks/${id}/assign`);
      await dispatch(getTrucks());
    } catch (err) {
      console.log(err);

      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const addTruck = (truckType) => {
  return async (dispatch) => {
    try {
      await axios.post(
        `/api/trucks`,
        {
          type: truckType,
        },
        {
          'Content-Type': 'application/json',
        }
      );
      await dispatch(getTrucks());
    } catch (err) {
      console.log(err);

      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const editTruck = (id, truckType) => {
  return async (dispatch) => {
    try {
      await axios.put(`/api/trucks/${id}`, { type: truckType });
      await dispatch(getTrucks());
    } catch (err) {
      console.log(err);

      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};
