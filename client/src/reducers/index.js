import { combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import loadsReducer from './loadsReducer/loadsReducer';
import trucksReducer from './trucksReducer/trucksReducer';
import loadingReducer from './loadingReducer/loadingReducer';
import errorReducer from './errorReducer/errorReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers({
  loadsReducer,
  trucksReducer,
  loadingReducer,
  errorReducer,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
