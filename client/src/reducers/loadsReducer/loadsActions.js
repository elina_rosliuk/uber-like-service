import axios from 'axios';
import { setErrorMessage } from '../errorReducer/errorActions';
import { setIsLoading } from '../loadingReducer/loadingReducer';
import { setActiveLoad, setLoads } from './loadsReducer';

export const getLoads = () => {
  return async (dispatch) => {
    dispatch(setIsLoading(true));

    try {
      const response = await axios.get(`/api/loads`);
      dispatch(setLoads(response.data.loads));
    } catch (err) {
      dispatch(setIsLoading(false));
      console.log(err);
    }

    dispatch(setIsLoading(false));
  };
};

export const getActiveLoad = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get(`/api/loads/active`);
      dispatch(setActiveLoad(response.data.load));
      // setIsLoading(false);
    } catch (err) {
      console.log(err);
      // setIsLoading(false);
    }
  };
};

export const deleteLoad = (id) => {
  return async (dispatch) => {
    try {
      await axios.delete(`/api/loads/${id}`);
      await dispatch(getLoads());
    } catch (err) {
      console.log(err);
      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const editLoad = (loadId, data) => {
  return async (dispatch) => {
    try {
      await axios.put(`/api/loads/${loadId}`, data, {
        'Content-Type': 'application/json',
      });
      dispatch(getLoads());
    } catch (err) {
      console.log(err);
      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const createLoad = (data) => {
  return async (dispatch) => {
    try {
      await axios.post(`/api/loads`, data, {
        'Content-Type': 'application/json',
      });
      dispatch(getLoads());
    } catch (err) {
      console.log(err);
      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const iterateToNextStatus = () => {
  return async (dispatch) => {
    console.log('iterate');
    try {
      const response = await axios.patch(`/api/loads/active/state`);

      if (response.data?.arrived) {
        dispatch(setActiveLoad(null));
        dispatch(setErrorMessage('Load arrived successfully'));
      }

      await dispatch(getLoads());
      await dispatch(getActiveLoad());
    } catch (err) {
      console.log(err);

      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};

export const postLoad = (id) => {
  return async (dispatch) => {
    try {
      const response = await axios.post(`/api/loads/${id}/post`);
      console.log('response ', response.data.message);

      // TODO: consider to refactor for not-error messages
      dispatch(setErrorMessage(response.data.message));
      dispatch(getLoads());
    } catch (err) {
      console.log(err);
      if (err.response) {
        dispatch(
          setErrorMessage(err.response?.data?.message, err.response.status)
        );
      }
    }
  };
};
