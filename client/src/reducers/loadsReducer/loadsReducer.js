const SET_LOADS = 'SET_LOADS';
const SET_ACTIVE_LOAD = 'SET_ACTIVE_LOAD';

const defaultState = {
  loads: [],
  activeLoad: null,
};

export default function loadsReducer(state = defaultState, action) {
  switch (action.type) {
    case SET_LOADS:
      return {
        ...state,
        loads: action.payload,
      };
    case SET_ACTIVE_LOAD:
      return {
        ...state,
        activeLoad: action.payload,
      };
    default:
      return state;
  }
}

export const setLoads = (loads) => ({ type: SET_LOADS, payload: loads });
export const setActiveLoad = (load) => ({
  type: SET_ACTIVE_LOAD,
  payload: load,
});
