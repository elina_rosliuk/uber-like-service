const SET_IS_LOADING = 'SET_IS_LOADING';

const defaultState = {
  isLoading: false,
};

export default function loadingReducer(state = defaultState, action) {
  switch (action.type) {
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
}

export const setIsLoading = (boolean) => ({
  type: SET_IS_LOADING,
  payload: boolean,
});
