export const tableHeaderData = [
  { title: 'Name' },
  { title: 'Created At' },
  { title: 'Status' },
  { title: 'State' },
  { title: 'Manage' },
];

export const tableHeaderData2 = [
  { title: 'Name' },
  { title: 'Created At' },
  { title: 'Assigned' },
  { title: 'Status' },
  { title: 'State' },
  { title: 'Manage' },
];

export const formData = [
  { label: 'Name', type: 'text', name: 'name' },
  { label: 'Payload', type: 'number', name: 'payload' },
  { label: 'Pickup Address', type: 'text', name: 'pickup_address' },
  { label: 'Delivery Address', type: 'text', name: 'delivery_address' },
  {
    label: 'Dimensions',
    name: 'dimensions',
    children: [
      { type: 'number', placeholder: 'Width', name: 'width' },
      { type: 'number', placeholder: 'Length', name: 'length' },
      { type: 'number', placeholder: 'Height', name: 'height' },
    ],
  },
];
