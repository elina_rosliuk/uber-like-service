export const trucksData = [
  { name: 'Sprinter' },
  { name: 'Small Straight' },
  { name: 'Large Straight' },
];

export const tableHeaderData = [
  { title: 'Type' },
  { title: 'Created At' },
  { title: 'Assigned' },
  { title: 'Manage' },
];
