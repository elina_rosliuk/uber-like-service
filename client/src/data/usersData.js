export const rolesData = [{ name: 'Shipper' }, { name: 'Driver' }];

export const profileMenuData = [
  { title: 'Profile info', icon: 'face', link: '/users/me' },
  { title: 'Change password', icon: 'lock', link: '/users/change-password' },
];

export const changePasswordFormData = [
  { id: 'old-password', title: 'Old password', name: 'oldPassword' },
  { id: 'new-password', title: 'New password', name: 'newPassword' },
  { id: 'repeat-new-password', title: 'Repeat new password', name: 'repeatNewPassword' },
];

export const authInputData = [
  { type: 'email', placeholder: 'Your email', autoComplete: 'off' },
  { type: 'password', placeholder: 'Your password' },
];
