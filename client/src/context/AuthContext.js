import React, { useState, createContext, useEffect } from 'react';
import axios from 'axios';
import setAuthToken from '../helpers/setAuthToken';
import { useHistory } from 'react-router-dom';

export const AuthContext = createContext();

const AuthState = (props) => {
  const [user, setUser] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(
    !!localStorage.AuthToken
  );

  const history = useHistory();

  const logout = () => {
    localStorage.removeItem('AuthToken');
    localStorage.removeItem('userData');
    setUser(null);
    setIsAuthenticated(false);
    history.push('/auth/login');
  };

  // Login user
  const loadUser = async () => {
    try {
      const {
        data: { user },
      } = await axios.get(`/api/users/me`, {
        'Content-Type': 'application/json',
      });
      localStorage.setItem('userData', JSON.stringify(user));
      setUser(user);
    } catch {
      logout();
    }
  };

  // Set data if login
  const login = async (token) => {
    setAuthToken(token);
    localStorage.setItem('AuthToken', token);
    setIsAuthenticated(true);
    await loadUser();
  };

  //Set headers
  if (localStorage.AuthToken) {
    setAuthToken(localStorage.AuthToken);
  }

  // Get user data after pager reload
  useEffect(() => {
    if (localStorage.AuthToken) {
      setIsAuthenticated(true);
    }

    if (localStorage.userData) {
      const user = JSON.parse(localStorage.userData);
      setUser(user);
    }

    if (!isAuthenticated) {
      history.push('/auth/login');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        login,
        user,
        logout,
        loadUser,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
