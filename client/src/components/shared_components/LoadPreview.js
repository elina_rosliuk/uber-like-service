import React from 'react';
import Preview from './Preview';

export const LoadPreview = ({
  open,
  setOpen,
  name,
  id,
  assigned_to,
  status,
  state,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
}) => {
  return (
    <Preview open={open} setOpen={setOpen}>
      <>
        <h2>
          Load "{name}" with id {id}
        </h2>
        <p>
          Assigned:{' '}
          {assigned_to
            ? `Assigned to driver with id ${assigned_to}`
            : 'Not assigned yet'}
        </p>
        <p>Status: {status}</p>
        <p>State: {state || 'N/A'}</p>
        <p>Payload: {payload}</p>
        <p>Pickup Address: {pickup_address}</p>
        <p>Delivery Address: {delivery_address}</p>
        <p>
          Dimensions: {dimensions.width}x{dimensions.length}x{dimensions.height}
        </p>
      </>
    </Preview>
  );
};
