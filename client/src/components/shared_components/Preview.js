import React from 'react';
import ReactDOM from 'react-dom';

export default function Preview({ open, setOpen, children }) {
  if (!open) return null;

  const handleClose = () => setOpen(false);

  return ReactDOM.createPortal(
    <>
      <div className="overlay" />
      <div className="modal-preview">
        <button className="modal-preview-close-button" onClick={handleClose}>
          <span className="material-icons material-icons-outlined">close</span>
        </button>
        <div className="modal-preview-content">{children}</div>
      </div>
    </>,
    document.getElementById('portal')
  );
}
