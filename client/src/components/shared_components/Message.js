import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeError } from '../../reducers/errorReducer/errorReducer';

export default function Message() {
  const dispatch = useDispatch();
  const error = useSelector((state) => state.errorReducer.error);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (error) {
      setOpen(true);
    }
  }, [error]);

  if (!open) return null;

  const handleClose = () => {
    setOpen(false);
    dispatch(removeError());
  };

  return ReactDOM.createPortal(
    <>
      {open && (
        <>
          <div className="overlay" />
          <div className="modal-error">
            <button className="modal-error-close-button" onClick={handleClose}>
              <span className="material-icons material-icons-outlined">
                close
              </span>
            </button>
            <div className="modal-error-content-block">
              <p className="modal-error-content">{error}</p>
            </div>
          </div>
        </>
      )}
    </>,
    document.getElementById('portal')
  );
}
