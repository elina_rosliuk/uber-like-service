import React from 'react';

export const Dropdown = ({ data, className = '', id = '', name, onChange = '', selected = '' }) => {
  const optionsList = data.map((item) => (
    <option value={item.name.toUpperCase()} key={item.name}>
      {item.name}
    </option>
  ));

  return (
    <select className={className} size="1" id={id} name={name} onChange={onChange} value={selected}>
      {optionsList}
    </select>
  );
};
