import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

export const Navbar = () => {
  const { user, logout } = useContext(AuthContext);

  const handleLogout = () => {
    logout();
  };

  return (
    <nav className="navbar">
      <Link className="logo" to="/dashboard">
        Logo
      </Link>
      <ul className="navbar-menu">
        {user?.role === 'DRIVER' ? (
          <>
            <Link className="navbar-link" to="/loads/active">
              Active Loads
            </Link>
            <Link className="navbar-link" to="/trucks">
              Trucks
            </Link>
          </>
        ) : (
          <Link className="navbar-link" to="/loads">
            Loads
          </Link>
        )}
        <Link className="navbar-link" to="/users/me">
          Profile
        </Link>
        <button className="navbar-logout" onClick={handleLogout}>
          Logout
        </button>
      </ul>
    </nav>
  );
};
