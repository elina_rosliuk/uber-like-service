import React from 'react';

export const IconButton = ({className, handleClick, icon, disabled = false, id = '' }) => (
  <button className={className} onClick={handleClick} disabled={disabled}>
    <span id={id} className="material-icons material-icons-outlined">
      {icon}
    </span>
  </button>
);
