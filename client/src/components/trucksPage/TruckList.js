import React from 'react';
import { Truck } from './Truck';
import { tableHeaderData } from '../../data/trucksData';

export const TruckList = ({ truckData }) => {
  const shouldButtonsBeDisabled = !!truckData.find(
    (truck) => truck.status === 'OL'
  );

  const truckList = truckData.map((truck) => (
    <Truck
      key={truck._id}
      id={truck._id}
      type={truck.type}
      assignedTo={truck.assigned_to}
      createdDate={truck.created_date}
      status={truck.status}
      shouldButtonsBeDisabled={shouldButtonsBeDisabled}
    />
  ));

  return (
    <table>
      <thead className="tbl-header">
        <tr>
          {tableHeaderData.map((header) => (
            <th key={header.title}>{header.title}</th>
          ))}
        </tr>
      </thead>
      <tbody>{truckList}</tbody>
    </table>
  );
};
