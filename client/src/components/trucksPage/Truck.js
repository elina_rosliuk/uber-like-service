import React, { useState } from 'react';
import TruckPreview from '../shared_components/Preview';
import { IconButton } from '../shared_components/IconButton';
import { useDispatch } from 'react-redux';
import { assignTruck, deleteTruck, editTruck } from '../../reducers/trucksReducer/truckActions';
import { Dropdown } from '../shared_components/Dropdown';
import { trucksData } from '../../data/trucksData';

export const Truck = ({ id, type, createdDate, assignedTo, status, shouldButtonsBeDisabled }) => {
  const dispatch = useDispatch();
  const [isPreview, setIsPreview] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [truckType, setTruckType] = useState(type);

  const handleFormChange = (event) => {
    setTruckType(event.target.value);
  };

  const handleAssign = () => {
    if (!assignedTo) {
      dispatch(assignTruck(id));
    }
  };

  const handleDelete = () => {
    dispatch(deleteTruck(id));
  };

  const handleEdit = () => {
    if (isEditing) {
      setIsEditing(false);
      dispatch(editTruck(id, truckType)).catch(() => setTruckType(type));
    } else {
      setIsEditing(true);
    }
  };

  const handlePreview = () => {
    setIsPreview(true);
  };

  return (
    <>
      <TruckPreview open={isPreview} setOpen={setIsPreview}>
        <h2>Truck with id {id}</h2>
        <p>
          Assigned: <span>{assignedTo ? 'Assigned' : 'Not Assigned'}</span>
        </p>
        <p>
          Type: <span>{type}</span>
        </p>
        <p>
          Status: <span>{status === 'IS' ? 'Free' : 'On Delivery'}</span>
        </p>
        <p>
          Created at: <span>{new Date(createdDate).toLocaleDateString()}</span>
        </p>
      </TruckPreview>

      <tr id={id}>
        {isEditing ? (
          <td>
            <Dropdown
              onChange={handleFormChange}
              name="truckType"
              id="truckType-dropdown"
              className="truck-dropdown"
              selected={truckType}
              data={trucksData}
            />
          </td>
        ) : (
          <td>{truckType}</td>
        )}
        <td>{new Date(createdDate).toLocaleDateString()}</td>
        <td>{assignedTo ? 'Assigned now' : 'Not assigned'}</td>
        <td className="table-manage-buttons">
          <button
            className="manage-button"
            id="assign-truck-button"
            onClick={handleAssign}
            disabled={shouldButtonsBeDisabled}
          >
            Assign Truck
          </button>
          <IconButton
            className="manage-button"
            handleClick={handleEdit}
            icon={isEditing ? 'done' : 'edit'}
            disabled={shouldButtonsBeDisabled}
          />
          <IconButton
            className="manage-button"
            handleClick={handleDelete}
            icon="delete"
            disabled={shouldButtonsBeDisabled}
          />
          <IconButton className="manage-button" handleClick={handlePreview} icon="remove_red_eye" />
        </td>
      </tr>
    </>
  );
};
