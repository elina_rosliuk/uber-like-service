import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTruck } from '../../reducers/trucksReducer/truckActions';
import { Dropdown } from '../shared_components/Dropdown';
import { trucksData } from '../../data/trucksData';

export const AddTruck = ({ getTrucks }) => {
  const dispatch = useDispatch();
  const [truckType, setTruckType] = useState('SPRINTER');

  const handleFormChange = (event) => {
    setTruckType(event.target.value);
  };

  const handleAddTruck = () => {
    dispatch(addTruck(truckType));
  };

  return (
    <form className="trucks-add-block">
      <div className="trucks-add-select">
        <label className="trucks-add-select-label" htmlFor="truckType-dropdown">
          Track type:
        </label>
        <Dropdown
          onChange={handleFormChange}
          name="truckType"
          id="truckType-dropdown"
          className="truck-dropdown"
          data={trucksData}
        />
      </div>
      <button className="trucks-add-button" onClick={handleAddTruck}>
        Add new truck
      </button>
    </form>
  );
};
