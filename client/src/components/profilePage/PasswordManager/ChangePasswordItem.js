import React, { useState } from 'react';

export const ChangePasswordItem = ({ item, onChange, value }) => {
  const [showPassword, setShowPassword] = useState(false);

  const handleShowPassword = (event) => {
    event.preventDefault();
    const newShowPassword = !showPassword;
    setShowPassword(newShowPassword);
  };

  return (
    <div className="change-password-block">
      <label htmlFor={item.id}>{item.title}</label>
      <input
        className="change-password-input"
        id={item.id}
        name={item.name}
        type={showPassword ? 'text' : 'password'}
        onChange={onChange}
        value={value}
      />
      <button className="show-password-button" onClick={handleShowPassword}>
        <span className="material-icons material-icons-outlined">
          {showPassword ? 'visibility' : 'visibility_off'}
        </span>
      </button>
    </div>
  );
};
