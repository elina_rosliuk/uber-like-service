import axios from 'axios';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { changePasswordFormData } from '../../../data/usersData';
import { useCenteredInput } from '../../../helpers/centeredInputHook';
import { setErrorMessage } from '../../../reducers/errorReducer/errorActions';
import { ChangePasswordItem } from './ChangePasswordItem';
import './passwordManagerStyles.css';

export const PasswordManager = () => {
  const formManager = useCenteredInput({
    oldPassword: '',
    newPassword: '',
    repeatNewPassword: '',
  });

  const dispatch = useDispatch();

  const formData = changePasswordFormData.map((item) => (
    <ChangePasswordItem
      key={item.id}
      item={item}
      value={formManager.form[item.name]}
      onChange={formManager.handleChange}
    />
  ));

  const requestPasswordChange = async (data) => {
    return await axios.patch('/api/users/me/password', data, {
      'Content-Type': 'application/json',
    });
  };

  const handleSubmitChangePassword = (event) => {
    event.preventDefault();

    let isError = formManager.validateForm();
    if (isError) return formManager.setError(isError);

    if (formManager.form?.newPassword !== formManager.form?.repeatNewPassword) {
      return formManager.setError('Passwords do not match.');
    }

    requestPasswordChange({
      oldPassword: formManager.form.oldPassword,
      newPassword: formManager.form.newPassword,
    })
      .then((response) => {
        dispatch(setErrorMessage(response.data.message));
        formManager.cleanForm();
        formManager.setError(null);
      })
      .catch((error) => {
        if (error.response.status >= 500) {
          formManager.setError('Server error');
        }

        if (error.response.status >= 400) {
          error.response?.data?.message
            ? formManager.setError(error.response.data.message)
            : formManager.setError('Client error');
        }
      });
  };

  return (
    <>
      <h3 className="profile-change-password-title">Change password</h3>
      <form className="profile-change-password-form">
        {formData}
        <div className="message-block">
          {formManager.error && <p className="message-error">*{formManager.error}</p>}
        </div>
        <button className="profile-change-password-button" onClick={handleSubmitChangePassword}>
          Change password
        </button>
      </form>
    </>
  );
};
