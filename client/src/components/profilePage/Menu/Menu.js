import React from 'react';
import { Link } from 'react-router-dom';
import './menuStyles.css';
import { profileMenuData } from '../../../data/usersData';

export const Menu = () => {
  const menuItems = profileMenuData.map((item) => (
    <Link key={item.title} className="profile-menu-item" to={item.link}>
      <span className="material-icons material-icons-outlined">{item.icon}</span>
      <span className="profile-menu-item-text">{item.title}</span>
    </Link>
  ));

  return <menu className="profile-menu">{menuItems}</menu>;
};
