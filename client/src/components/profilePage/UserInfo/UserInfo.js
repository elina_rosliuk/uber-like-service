import React from 'react';

export const UserInfo = () => {
  const { _id: userId, created_date: createdDate, email } = JSON.parse(
    localStorage.getItem('userData')
  );
  return (
    <>
      <p>
        User id: <span>{userId}</span>
      </p>
      <p>
        Your email: <span>{email}</span>
      </p>
      <p>
        Profile was created at: <span>{new Date(createdDate).toLocaleDateString()}</span>
      </p>
    </>
  );
};
