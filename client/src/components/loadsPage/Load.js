import React, { useState, useContext } from 'react';
import { AuthContext } from '../../context/AuthContext';
import { Link } from 'react-router-dom';
import { IconButton } from '../shared_components/IconButton';
import { deleteLoad, postLoad } from '../../reducers/loadsReducer/loadsActions';
import { useDispatch } from 'react-redux';
import { LoadPreview } from '../shared_components/LoadPreview';

export const Load = ({
  id,
  name,
  created_date,
  assigned_to,
  status,
  state,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
  logs,
}) => {
  const dispatch = useDispatch();
  const { user } = useContext(AuthContext);
  const [isPreview, setIsPreview] = useState(false);

  const shouldBeButtonDisabled = status === 'ASSIGNED';

  const handleDelete = () => {
    dispatch(deleteLoad(id));
  };

  const handlePreview = () => {
    setIsPreview(true);
  };

  const handlePostLoad = () => {
    dispatch(postLoad(id));
  };

  return (
    <>
      <LoadPreview
        open={isPreview}
        setOpen={setIsPreview}
        name={name}
        id={id}
        assigned_to={assigned_to}
        status={status}
        state={state}
        payload={payload}
        pickup_address={pickup_address}
        delivery_address={delivery_address}
        dimensions={dimensions}
      />

      <tr id={id}>
        <td>{name}</td>
        <td>{new Date(created_date).toLocaleDateString()}</td>
        <td>{assigned_to ? 'Assigned now' : 'Not assigned'}</td>
        <td>{status}</td>
        <td>{state || 'N/A'}</td>
        <td className="table-manage-buttons">
          {user?.role === 'SHIPPER' && (
            <>
              {status === 'NEW' && (
                <button
                  className="manage-button"
                  id="post-load-button"
                  onClick={handlePostLoad}
                >
                  Post Load
                </button>
              )}
              <Link
                to={{
                  pathname: '/loads/form',
                  state: {
                    isEditing: true,
                    loadId: id,
                    data: {
                      name,
                      payload,
                      pickup_address,
                      delivery_address,
                      dimensions,
                    },
                  },
                }}
                className="manage-button"
                style={
                  shouldBeButtonDisabled
                    ? {
                        pointerEvents: 'none',
                        backgroundColor: 'rgba(141, 141, 141, 0.6)',
                      }
                    : null
                }
              >
                <span className="material-icons material-icons-outlined">
                  edit
                </span>
              </Link>
              <IconButton
                className="manage-button"
                handleClick={handleDelete}
                icon="delete"
                disabled={shouldBeButtonDisabled}
              />
            </>
          )}
          <IconButton
            className="manage-button"
            handleClick={handlePreview}
            icon="remove_red_eye"
          />
        </td>
      </tr>
    </>
  );
};
