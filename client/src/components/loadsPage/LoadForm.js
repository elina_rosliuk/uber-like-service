import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { LoadFormInput } from './LoadFormInput';
import { editLoad, createLoad } from '../../reducers/loadsReducer/loadsActions';
import { useDispatch } from 'react-redux';
import { useInput } from '../../helpers/inputHook';
import { formData } from '../../data/loadsData';

export const LoadForm = (props) => {
  const dispatch = useDispatch();
  const [isEditing] = useState(!!props?.location.state?.isEditing);
  const [loadData] = useState(props?.location?.state?.data || {});
  const history = useHistory();

  const inputState = {
    name: useInput(loadData.name || '', { isEmpty: false }),
    payload: useInput(loadData.payload || 0, { isNegativeNumber: false }),
    pickup_address: useInput(loadData.pickup_address || '', { isEmpty: false }),
    delivery_address: useInput(loadData.delivery_address || '', {
      isEmpty: false,
    }),
    width: useInput(loadData.dimensions?.width || '', {
      isNegativeNumber: false,
    }),
    length: useInput(loadData.dimensions?.length || '', {
      isNegativeNumber: false,
    }),
    height: useInput(loadData.dimensions?.height || '', {
      isNegativeNumber: false,
    }),
  };

  const handleSubmit = (event) => {
    const data = {};

    for (let [key, item] of Object.entries(inputState)) {
      if (key === 'width' || key === 'height' || key === 'length') {
        data['dimensions'] = { ...data['dimensions'], [key]: item.value };
      } else {
        data[key] = item.value;
      }
    }

    event.preventDefault();
    isEditing ? dispatch(editLoad(props.location.state.loadId, data)) : dispatch(createLoad(data));

    history.push('/loads');
  };

  const handleCancel = (event) => {
    event.preventDefault();
    history.push('/loads');
  };

  const formInputs = formData.map((item) => (
    <LoadFormInput key={item.name} item={item} inputState={inputState} />
  ));

  const isFormValid = () => {
    for (let key of Object.values(inputState)) {
      if (!key.isValid) return true;
    }

    return false;
  };

  return (
    <form className="loadForm">
      <h2 className="content-title">{isEditing ? 'Edit Load' : 'New Load'}</h2>

      {formInputs}

      <div className="loadForm-button-block">
        <button className="loadForm-button" onClick={handleSubmit} disabled={isFormValid()}>
          {isEditing ? 'Save changes' : 'Add New Load'}
        </button>
        <button className="loadForm-button" onClick={handleCancel}>
          Cancel
        </button>
      </div>
    </form>
  );
};
