import React from 'react';
import { Load } from './Load';
import { tableHeaderData2 as tableHeaderData } from '../../data/loadsData'

export const LoadsList = ({ loadsData }) => {
  const loadsList = loadsData.map((load) => (
    <Load
      key={load._id}
      id={load._id}
      name={load.name}
      assigned_to={load.assigned_to}
      created_date={load.created_date}
      status={load.status}
      state={load.state}
      payload={load.payload}
      pickup_address={load.pickup_address}
      delivery_address={load.delivery_address}
      dimensions={load.dimensions}
      logs={load.logs}
    />
  ));

  return (
    <table>
      <thead className="tbl-header">
        <tr>
          {tableHeaderData.map((header) => (
            <th key={header.title}>{header.title}</th>
          ))}
        </tr>
      </thead>
      <tbody>{loadsList}</tbody>
    </table>
  );
};
