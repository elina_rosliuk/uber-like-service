import React from 'react';
import { Link } from 'react-router-dom';

export const NewLoadButton = () => (
  <div className="new-load-button-block">
    <Link
      to={{ pathname: '/loads/form', state: { isEditing: false } }}
      className="new-load-button"
    >
      Add New Load
    </Link>
  </div>
);
