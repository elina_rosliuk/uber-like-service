import React, { useState } from 'react';
import { IconButton } from '../shared_components/IconButton';
import { useDispatch, useSelector } from 'react-redux';
import { iterateToNextStatus } from '../../reducers/loadsReducer/loadsActions';
import { LoadPreview } from '../shared_components/LoadPreview';
import { tableHeaderData } from '../../data/loadsData';

export const ActiveLoadCard = () => {
  const [isPreview, setIsPreview] = useState(false);
  const dispatch = useDispatch();
  const loadData = useSelector((state) => state.loadsReducer.activeLoad);

  const handleNextStatusIteration = () => {
    dispatch(iterateToNextStatus());
  };

  return (
    <>
      <LoadPreview
        open={isPreview}
        setOpen={setIsPreview}
        name={loadData.name}
        id={loadData.id}
        assigned_to={loadData.assigned_to}
        status={loadData.status}
        state={loadData.state}
        payload={loadData.payload}
        pickup_address={loadData.pickup_address}
        delivery_address={loadData.delivery_address}
        dimensions={loadData.dimensions}
      />

      {loadData && (
        <table>
          <thead className="tbl-header">
            <tr>
              {tableHeaderData.map((header) => (
                <th key={header.title}>{header.title}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{loadData.name}</td>
              <td>{new Date(loadData.created_date).toLocaleDateString()}</td>
              <td>{loadData.status}</td>
              <td>{loadData.state ? loadData.state : 'N/A'}</td>
              <td className="table-manage-buttons">
                <button
                  className="manage-button next-status-button"
                  onClick={handleNextStatusIteration}
                >
                  Next Status
                </button>
                <IconButton
                  className="manage-button"
                  icon="remove_red_eye"
                  handleClick={() => {
                    setIsPreview(true);
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      )}
    </>
  );
};
