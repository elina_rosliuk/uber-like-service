import React from 'react';

export const LoadFormInput = ({ item, inputState }) => {
  if (item.children) {
    return (
      <>
        <label>
          {item.label}
          {item.children.map((child) => (
            <React.Fragment key={`${child.name}`}>
              {inputState[child.name]?.isError && inputState[child.name]?.isUsed ? (
                <div className="loadForm-input-error">{inputState[child.name].isError}</div>
              ) : null}
              <input
                className="loadForm-input"
                type={child.type}
                placeholder={child.placeholder}
                min={1}
                name={child.name}
                value={inputState[child.name].value}
                onChange={inputState[child.name].handleChange}
                onBlur={inputState[child.name].handleBlur}
                required
              />
            </React.Fragment>
          ))}
        </label>
      </>
    );
  } else {
    return (
      <>
        <label>
          {inputState[item.name]?.isError && inputState[item.name]?.isUsed ? (
            <div className="loadForm-input-error">{inputState[item.name].isError}</div>
          ) : null}
          {item.label}
          <input
            id={`${item.name}-load-form-input`}
            type={item.type}
            min={item.type === 'number' ? 1 : ''}
            name={item.name}
            value={inputState[item.name].value}
            onChange={inputState[item.name].handleChange}
            onBlur={inputState[item.name].handleBlur}
            className="loadForm-input"
            required
          />
        </label>
      </>
    );
  }
};
