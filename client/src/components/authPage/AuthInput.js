import React from 'react';

export const AuthInput = ({ onChange, type, placeholder, autoComplete = 'on' }) => (
  <input
    className="auth-input"
    id={type}
    name={type}
    type={type}
    placeholder={placeholder}
    autoComplete={autoComplete}
    onChange={onChange}
    required={true}
  />
);
