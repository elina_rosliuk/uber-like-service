import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { LoginPage } from '../pages/authPage/LoginPage';
import { RegisterPage } from '../pages/authPage/RegisterPage';
import { Dashboard } from '../pages/dashboard/Dashboard';

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/">
          <Dashboard />
        </Route>
        <Route path="*" exact>
          <Redirect to="/" />
        </Route>
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path="/auth/register" exact>
        <RegisterPage />
      </Route>
      <Route path="/auth/login" exact>
        <LoginPage />
      </Route>
      <Route path="*" exact>
        <Redirect to="/auth/login" />
      </Route>
    </Switch>
  );
};
