import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

export const RolesRoute = ({ path, component, role }) => {
  const { user } = useContext(AuthContext);

  if (role === user?.role) {
    return <Route path={path} component={component} exact />
  }  

  return null;
}