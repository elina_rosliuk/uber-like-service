import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTrucks } from '../../reducers/trucksReducer/truckActions';
import { Spinner } from '../../components/shared_components/Spinner';
import { TruckList } from '../../components/trucksPage/TruckList';
import { AddTruck } from '../../components/trucksPage/AddTruck';
import '../../components/mainPages_generalStyles.css';
import '../../components/trucksPage/truckStyles.css';

export const TrucksPage = () => {
  const dispatch = useDispatch();
  const trucksData = useSelector((state) => state.trucksReducer.trucks);
  const isLoading = useSelector((state) => state.loadingReducer.isLoading);

  useEffect(() => {
    dispatch(getTrucks());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="content">
      <h2 className="content-title">My trucks</h2>

      <AddTruck getTrucks={getTrucks} />

      {isLoading ? (
        <Spinner />
      ) : trucksData.length ? (
        <TruckList truckData={trucksData} />
      ) : (
        <p>No trucks found</p>
      )}
    </div>
  );
};
