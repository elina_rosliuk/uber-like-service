import React, { useState } from 'react';
import axios from 'axios';
import { useHistory, Link } from 'react-router-dom';
import { AuthInput } from '../../components/authPage/AuthInput';
import '../../components/authPage/authPage.css';
import { Dropdown } from '../../components/shared_components/Dropdown';
import { authInputData, rolesData } from '../../data/usersData';
import { useCenteredInput } from '../../helpers/centeredInputHook';

export const RegisterPage = () => {
  const dataManager = useCenteredInput({
    email: '',
    password: '',
    role: 'SHIPPER',
  });

  const history = useHistory();

  const handleRegister = async (event) => {
    event.preventDefault();

    let isNotValid = dataManager.validateForm(); // TODO: consider better way to validate data

    if (isNotValid) return dataManager.setError(isNotValid);

    axios
      .post('/api/auth/register', { ...dataManager.form }, { 'Content-Type': 'application/json' })
      .then(() => history.push('/auth/login'))
      .catch(function (error) {
        console.log(error);

        if (error.response.status >= 500) {
          dataManager.setError('Server error');
        }

        if (error.response.status >= 400) {
          error.response?.data?.message
            ? dataManager.setError(error.response.data.message)
            : dataManager.setError('Client error');
        }
      });
  };

  const authInputs = authInputData.map((item) => (
    <React.Fragment key={item.placeholder}>
      <AuthInput
        type={item.type}
        placeholder={item.placeholder}
        autoComplete={item.autoComplete}
        onChange={dataManager.handleChange}
      />
      <br />
    </React.Fragment>
  ));

  return (
    <div className="auth">
      <div className="auth-window">
        <form className="auth-form">
          {authInputs}
          <div>
            <label htmlFor="role">
              I am:
              <Dropdown
                data={rolesData}
                className="role-dropdown"
                name="role"
                onChange={dataManager.handleChange}
              />
            </label>
          </div>
          <br />

          <button className="submit-button" onClick={handleRegister}>
            Register
          </button>
          <div className="redirect-block">
            <p>
              Do you already have account?
              <Link className="auth-redirect-button" to="/auth/login">
                Log in
              </Link>
            </p>
          </div>
          <div className="auth-message-block">
            {dataManager.error && <p className="message-error">*{dataManager.error}</p>}
          </div>
        </form>
      </div>
    </div>
  );
};
