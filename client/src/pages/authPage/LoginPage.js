import React, { useContext, useState, useEffect } from 'react';
import axios from 'axios';
import { AuthContext } from '../../context/AuthContext';
import { useHistory, Link } from 'react-router-dom';
import { AuthInput } from '../../components/authPage/AuthInput';
import '../../components/authPage/authPage.css';
import { useCenteredInput } from '../../helpers/centeredInputHook';
import { authInputData } from '../../data/usersData';

/* TODO: Consider combining login and register pages and/or remove dublicates! */

export const LoginPage = () => {
  const { isAuthenticated, login } = useContext(AuthContext);
  const history = useHistory();

  const dataManager = useCenteredInput({
    email: '',
    password: '',
  });

  const handleLogin = async (event) => {
    event.preventDefault();

    let isNotValid = dataManager.validateForm(); // TODO: consider better way to validate data

    if (isNotValid) return dataManager.setError(isNotValid);

    axios
      .post('/api/auth/login', { ...dataManager.form }, { 'Content-Type': 'application/json' })
      .then(function (response) {
        login(response.data.jwt_token);
      })
      .catch(function (error) {
        console.log(error.response);

        if (error.response.status >= 500) {
          dataManager.setError('Server error');
        }

        if (error.response.status >= 400) {
          dataManager.setError('Wrong email or password');
        }
      });
  };

  useEffect(() => {
    if (isAuthenticated) {
      history.push('/');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthenticated]);

  const authInputs = authInputData.map((item) => (
    <React.Fragment key={item.placeholder}>
      <AuthInput
        type={item.type}
        placeholder={item.placeholder}
        autoComplete={item.autoComplete}
        onChange={dataManager.handleChange}
      />
      <br />
    </React.Fragment>
  ));

  return (
    <div className="auth">
      <div className="auth-window">
        <form className="auth-form">
          {authInputs}
          <button className="submit-button" onClick={handleLogin}>
            Login
          </button>
          <div className="redirect-block">
            <p>
              Do not have account yet?
              <Link className="auth-redirect-button" to="/auth/register">
                Register
              </Link>
            </p>
          </div>
          <div className="auth-message-block">
            {dataManager.error && <p className="message-error">*{dataManager.error}</p>}
          </div>
        </form>
      </div>
    </div>
  );
};
