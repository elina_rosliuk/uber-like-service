import React from 'react';
import { Route, Switch } from 'react-router';
import { Menu } from '../../components/profilePage/Menu/Menu';
import { PasswordManager } from '../../components/profilePage/PasswordManager/PasswordManager';
import { UserInfo } from '../../components/profilePage/UserInfo/UserInfo';
import './profileStyles.css';

export const ProfilePage = () => {
  return (
    <div className="profile-block">
      <h2 className="content-title">Profile information</h2>
      <div className="profile-content">
        <Menu />
        <div className="profile-user-info">
        <Switch>
          <Route path="/users/me" component={UserInfo} exact />
          <Route path="/users/change-password" component={PasswordManager} exact />
        </Switch>
        </div>
        
      </div>
    </div>
  );
};
