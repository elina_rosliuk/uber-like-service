import React from 'react';
import { Navbar } from '../../components/shared_components/Navbar';
import { Route, Switch } from 'react-router-dom';
import './dashboard.css';
import { ProfilePage } from '../profilePage/ProfilePage';
import { TrucksPage } from '../trucksPage/TrucksPage';
import { LoadsPage } from '../loadsPage/LoadsPage';
import { DriverLoadsPage } from '../loadsPage/DriverLoadsPage';
import { LoadForm } from '../../components/loadsPage/LoadForm';
import { RolesRoute } from '../../routes/RolesRoute';

export const Dashboard = () => {
  return (
    <div className="main-page">
      <div className="dashboard">
        <Navbar />
        <hr className="line" />

        <main className="main">
          <Switch>
            <Route path="/users" component={ProfilePage} />
            <RolesRoute
              role="DRIVER"
              path="/loads/active"
              component={DriverLoadsPage}
            />
            <RolesRoute role="DRIVER" path="/trucks" component={TrucksPage} />
            <RolesRoute
              role="SHIPPER"
              path="/loads/form"
              component={LoadForm}
            />
            <RolesRoute role="SHIPPER" path="/loads" component={LoadsPage} />
          </Switch>
        </main>
      </div>
    </div>
  );
};
