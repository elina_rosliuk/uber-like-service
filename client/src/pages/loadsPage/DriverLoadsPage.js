import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getActiveLoad,
  getLoads,
} from '../../reducers/loadsReducer/loadsActions';
import { ActiveLoadCard } from '../../components/loadsPage/ActiveLoadCard';
import '../../components/trucksPage/truckStyles.css';
import '../../components/loadsPage/loadsStyles.css';
import { LoadsList } from '../../components/loadsPage/LoadsList';
import { IconButton } from '../../components/shared_components/IconButton';

export const DriverLoadsPage = () => {
  const [collapsibleOpened, setCollapsibleOpened] = useState({
    history: false,
    active: false,
  });
  const dispatch = useDispatch();
  const { loads: loadsData, activeLoad } = useSelector(
    (state) => state.loadsReducer
  );

  useEffect(() => {
    dispatch(getActiveLoad());
    dispatch(getLoads());

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleExpandCollapsible = (event) => {
    const targetId = event.target.id;

    if (targetId && targetId.includes('collapsible')) {
      const collapsibleType = targetId.split('-')[0];

      setCollapsibleOpened({
        ...collapsibleOpened,
        [collapsibleType]: !collapsibleOpened[collapsibleType],
      });
    }
  };

  return (
    <div className="content">
      <h2 className="content-title">My loads</h2>

      <div className="driver-loads-block">
        <div className="driver-loads-collapsible">
          <IconButton
            id="active-collapsible"
            className="driver-loads-collapsible-button active"
            icon={collapsibleOpened.active ? 'expand_less' : 'expand_more'}
            handleClick={handleExpandCollapsible}
          />
          <span className="driver-loads-collapsible-title">Active</span>
        </div>
        {collapsibleOpened.active && (
          <>{activeLoad ? <ActiveLoadCard /> : <p>No active loads found</p>}</>
        )}
      </div>
      <div className="driver-loads-block">
        <div className="driver-loads-collapsible">
          <IconButton
            id="history-collapsible"
            className="driver-loads-collapsible-button"
            icon={collapsibleOpened.history ? 'expand_less' : 'expand_more'}
            handleClick={handleExpandCollapsible}
          />
          <span className="driver-loads-collapsible-title">History</span>
        </div>
        {collapsibleOpened.history && (
          <>
            {loadsData.length ? (
              <LoadsList loadsData={loadsData} getLoads={getLoads} />
            ) : (
              <p>No loads found</p>
            )}
          </>
        )}
      </div>
    </div>
  );
};
