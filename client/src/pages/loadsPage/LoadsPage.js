import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getLoads } from '../../reducers/loadsReducer/loadsActions';
import { Spinner } from '../../components/shared_components/Spinner';
import { LoadsList } from '../../components/loadsPage/LoadsList';
import '../../components/mainPages_generalStyles.css';
import '../../components/loadsPage/loadsStyles.css';
import { NewLoadButton } from '../../components/loadsPage/NewLoadButton';

export const LoadsPage = () => {
  const dispatch = useDispatch();
  const loadsData = useSelector((state) => state.loadsReducer.loads);
  const isLoading = useSelector((state) => state.loadingReducer.isLoading);

  useEffect(() => {
    dispatch(getLoads());

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="content">
      <h2 className="content-title">My loads</h2>

      <NewLoadButton />

      {isLoading ? (
        <Spinner />
      ) : loadsData.length ? (
        <LoadsList loadsData={loadsData} />
      ) : (
        <p>No loads found</p>
      )}
    </div>
  );
};
