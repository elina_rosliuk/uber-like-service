import React, { useContext } from 'react';
import { useRoutes } from './routes/routes';
import { BrowserRouter } from 'react-router-dom';
import { AuthContext } from './context/AuthContext';

function App() {
  const { isAuthenticated  } = useContext(AuthContext);

  const router = useRoutes(isAuthenticated);

  return (
      <div className="App">
        <BrowserRouter>
          {router}
        </BrowserRouter>
      </div>
  );
}

export default App;
