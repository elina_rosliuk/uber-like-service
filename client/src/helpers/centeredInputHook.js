import { useState } from 'react';

export const useCenteredInput = (initialValue) => {
  const [form, setForm] = useState(initialValue);
  const [error, setError] = useState(null);
  const initialData = initialValue;

  const handleChange = (event) => {
    setError(null);
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const cleanForm = () => {
    setForm(initialData);
    setError(null);
  };

  const validateForm = () => {
    let error;
    Object.values(form).forEach((item) => {
      if (!item.trim()) {
        error = 'All fields are required';
      }
    });

    return error;
  };

  return {
    form,
    handleChange,
    cleanForm,
    validateForm,
    error,
    setError,
  };
};
