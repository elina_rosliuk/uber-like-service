import { useEffect, useState } from 'react';

export const useValidation = (value, validations) => {
  const [isError, setIsError] = useState(false);
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    for (const validation in validations) {
      switch (validation) {
        case 'isEmpty':
          !value.trim()
            ? setIsError('This field cannot be empty!')
            : setIsError(false);
          break;
        case 'isNegativeNumber':
          value < 1
            ? setIsError('This field may contain only a positive number')
            : setIsError(false);
          break;
        default:
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  useEffect(() => {
    isError ? setIsValid(false) : setIsValid(true);
  }, [isError]);

  return {
    isError,
    isValid,
  };
};

export const useInput = (initialValue, validations) => {
  const [value, setValue] = useState(initialValue);
  const [isUsed, setIsUsed] = useState(false);
  const valid = useValidation(value, validations);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleBlur = () => {
    setIsUsed(true);
  };

  return {
    value,
    isUsed,
    handleChange,
    handleBlur,
    ...valid,
  };
};
