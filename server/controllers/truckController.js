const {Truck} = require('./../models/truckModel');
const {findTruckById} = require('./../routers/helpers/truckHelper');

module.exports.addTruck = async (req, res) => {
  const {type} = req.body;
  const userCredentials = req.user;

  const truck = new Truck({
    created_by: userCredentials._id,
    type,
  });

  truck.save();

  res.status(200).json({message: 'Truck created successfully'});
};

module.exports.getAllTrucks = async (req, res) => {
  const userCredentials = req.user;

  let trucks;

  try {
    trucks = await Truck.find({created_by: userCredentials._id})
        .select({'__v': 0});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({trucks});
};

module.exports.getTruck = async (req, res) => {
  const userCredentials = req.user;
  const {id} = req.params;

  const truck = await findTruckById(id, userCredentials._id);

  if (!truck) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({truck});
};

module.exports.updateTruck = async (req, res) => {
  const {type} = req.body;
  const {id} = req.params;
  const userCredentials = req.user;

  const truck = await findTruckById(id, userCredentials._id );

  if (!truck || truck.status === 'OL' ) {
    return res.status(400).json({message: 'Client error'});
  }

  await truck.updateOne({type});

  truck.save();

  res.status(200).json({message: 'Truck details changed successfully'});
};

module.exports.deleteTruck = async (req, res) => {
  const {id} = req.params;

  const isDeleted = await Truck.deleteOne({_id: id, status: 'IS'});

  if (!isDeleted.deletedCount) {
    return res.status(400).json({message: 'Truck was not deleted'});
  }

  res.status(200).json({message: 'Truck deleted successfully'});
};

module.exports.assignTruck = async (req, res) => {
  const {id} = req.params;
  const {_id: userId} = req.user;

  const previousAssigned = await Truck.findOne({
    created_by: userId,
    assigned_to: userId,
  }) || null;

  if (previousAssigned) {
    await previousAssigned.updateOne({assigned_to: null});
  }

  const currentAssigned = await Truck.findOne({_id: id, created_by: userId});

  if (!currentAssigned) {
    return res.status(400).json({message: 'No truck with this id is found'});
  }

  await currentAssigned.updateOne({assigned_to: userId});

  currentAssigned.save();

  return res.status(200).json({message: 'Truck assigned successfully'});
};
