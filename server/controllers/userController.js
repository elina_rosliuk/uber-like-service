const {User} = require('./../models/userModel');
const bcrypt = require('bcrypt');

module.exports.getProfile = async (req, res) => {
  const userCredentials = req.user;
  const user = await User.findById({_id: userCredentials._id});

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }

  res.status(200).json({user});
};

module.exports.deleteProfile = async (req, res) => {
  const userCredentials = req.user;

  try {
    await User.deleteOne({_id: userCredentials._id});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({message: 'Profile deleted successfully'});
};

module.exports.changePassword = async (req, res) => {
  const userCredentials = req.user;
  const {oldPassword, newPassword} = req.body;

  if (oldPassword === newPassword) {
    return res.status(400).json({
      message: 'New password should differ from old one',
    });
  }

  const user = await User.findById({_id: userCredentials._id});

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }

  const verified = await bcrypt.compare(oldPassword, user.password);

  if (!verified) {
    return res.status(400).json({message: 'Wrong current password'});
  }

  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});

  user.save();

  res.status(200).json({message: 'Password changed successfully.'});
};
