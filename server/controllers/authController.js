const passwordGenerator = require('generate-password');
const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWTSecret} = require('../config');

module.exports.login = async (req, res, next) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `No user with email '${email}' was found!`,
    });
  }

  const isPasswordCorrect = await bcrypt.compare(password, user.password);

  if (!isPasswordCorrect) {
    return res.status(400).json({message: 'Wrong password'});
  }

  const JWT_TOKEN = jwt.sign(
      {email: user.email, _id: user._id, role: user.role},
      JWTSecret,
  );

  res.header('authorization', JWT_TOKEN);

  res.status(200).json({jwt_token: JWT_TOKEN});
};

module.exports.register = async (req, res, next) => {
  const {email, password, role} = req.body;

  const isUserExist = await User.findOne({email});

  if (isUserExist) {
    return res.status(400).json({
      message: `User with email '${email}' already exists!`,
    });
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.status(200).json({message: 'Profile created successfully'});
};

module.exports.changePassword = async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `No user with email '${email}' was found!`,
    });
  }

  const newPassword = passwordGenerator.generate({
    length: 10,
    numbers: true,
  });

  console.log(newPassword);

  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});
  user.save();

  // Send password on the user's email
  /* ........ */

  // what if user finds out that he has no
  // access to the email and recalls in mind the old password ????

  res.status(200).json({message: 'New password sent to your email address'});
};
