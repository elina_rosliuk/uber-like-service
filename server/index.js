const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const {dbLink} = require('./config');

require('dotenv').config();

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

/** Class representing a status code */
class UnauthorizedError extends Error {
  /**
     *
     * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user!') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(
      dbLink,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
};

start();
