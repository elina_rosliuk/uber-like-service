const express = require('express');
const {
  login,
  register,
  changePassword,
} = require('./../controllers/authController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateRegistration} = require('./middlewares/validateRegistration');

const router = new express.Router();

router.post('/auth/register', validateRegistration, asyncWrapper(register));
router.post('/auth/login', asyncWrapper(login), authMiddleware);
router.post('/auth/forgot_password', asyncWrapper(changePassword));


module.exports = router;
