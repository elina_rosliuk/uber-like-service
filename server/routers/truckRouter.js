const express = require('express');
const {
  addTruck,
  getAllTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/truckController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {checkDriverMiddleware} = require('./middlewares/checkRoleMiddleware');
const {validateTruckType} = require('./middlewares/validateTruckType');
const router = new express.Router();

router.get(
    '/trucks',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getAllTrucks),
);
router.post(
    '/trucks',
    authMiddleware,
    checkDriverMiddleware,
    validateTruckType,
    asyncWrapper(addTruck),
);
router.get(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getTruck),
);
router.put(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    validateTruckType,
    asyncWrapper(updateTruck),
);
router.delete(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(deleteTruck),
);
router.post(
    '/trucks/:id/assign',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(assignTruck),
);

module.exports = router;
