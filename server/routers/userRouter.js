const express = require('express');
const {asyncWrapper} = require('./helpers/errorHelper');
const {
  getProfile,
  deleteProfile,
  changePassword,
} = require('./../controllers/userController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validatePassword} = require('./middlewares/validatePassword');
const router = new express.Router();

router.get('/users/me', authMiddleware, asyncWrapper(getProfile));
router.delete('/users/me', authMiddleware, asyncWrapper(deleteProfile));
router.patch(
    '/users/me/password',
    authMiddleware,
    validatePassword,
    asyncWrapper(changePassword),
);

module.exports = router;
