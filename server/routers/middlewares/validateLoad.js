const Joi = require('joi');

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().min(1).max(100000).required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object()
        .pattern(/.*/, [Joi.number(), Joi.number(), Joi.number()]),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    return res.status(400).json({message: 'Load data is not valid'});
  }

  next();
};
