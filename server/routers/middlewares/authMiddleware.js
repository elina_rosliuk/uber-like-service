const jwt = require('jsonwebtoken');
const {JWTSecret} = require('../../config');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({
      message: `No Authorization http header found!`,
    });
  }

  const token = header.split(' ')[1];

  if (!token) {
    return res.status(401).json({message: `No JWT token found!`});
  }

  const verified = jwt.verify(token, JWTSecret);

  req.user = verified;

  next();
};
