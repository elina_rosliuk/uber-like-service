const Joi = require('joi');

module.exports.validatePassword = async (req, res, next) => {
  const {newPassword} = req.body;

  const schema = Joi.object({
    password: Joi.string()
        .required()
        .min(6)
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .messages({
          'string.base': `Only letters and numbers can be used for password`,
          'string.empty': `Password cannot be empty`,
          'string.min': `Password should contain at least 6 symbols`,
          'string.pattern.base':
            'Password should contain only letters and numbers',
        }),
  });

  try {
    await schema.validateAsync({password: newPassword});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
