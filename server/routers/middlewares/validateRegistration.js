const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email({minDomainSegments: 2})
        .messages({
          'string.base': `Email is not valid`,
          'string.empty': `Email cannot be empty`,
          'string.email': `This email is not valid`,
        }),

    password: Joi.string().min(6).pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .messages({
          'string.base': `Only letters and numbers can be used for password`,
          'string.empty': `Password cannot be empty`,
          'string.min': `Password should contain at least 6 symbols`,
          'string.pattern.base':
            'Password should contain only letters and numbers',
        }),

    role: Joi.string().valid('DRIVER', 'SHIPPER'),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
