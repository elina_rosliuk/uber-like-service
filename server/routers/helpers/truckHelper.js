const {Truck} = require('./../../models/truckModel');

module.exports.findTruckById = async (id, driverId) => {
  return await Truck.findOne({created_by: driverId, _id: id})
      .select({'__v': 0});
};
