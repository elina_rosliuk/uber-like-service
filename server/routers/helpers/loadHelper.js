module.exports.updateState = async (load, state) => {
  await load.updateOne({state});

  load.save();

  return state;
};

module.exports.changeLoadStatus = async (load, status, message) => {
  await load.updateOne({status, logs: [...load.logs, {message}]});

  load.save();
};
