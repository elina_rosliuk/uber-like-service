const express = require('express');
const {
  getAllLoads,
  addLoad,
  getActiveLoad,
  changeLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadShippingInfo,
} = require('../controllers/loadController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  checkShipperMiddleware,
  checkDriverMiddleware,
} = require('./middlewares/checkRoleMiddleware');
const {
  validateLoad,
} = require('./middlewares/validateLoad');
const router = new express.Router();

router.get('/loads', authMiddleware, asyncWrapper(getAllLoads));
router.post(
    '/loads',
    authMiddleware,
    checkShipperMiddleware,
    validateLoad,
    asyncWrapper(addLoad),
);
router.get(
    '/loads/active',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getActiveLoad),
);
router.patch(
    '/loads/active/state',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(changeLoadState),
);
router.get('/loads/:id', authMiddleware, asyncWrapper(getLoad));
router.put(
    '/loads/:id',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(updateLoad),
);
router.delete(
    '/loads/:id',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(deleteLoad),
);
router.post(
    '/loads/:id/post',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(postLoad),
);
router.get(
    '/loads/:id/shipping_info',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(getLoadShippingInfo),
);

module.exports = router;
