const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    default: 'IS',
    enum: ['IS', 'OL'],
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
